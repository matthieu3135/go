package main

import (
    "fmt"
    "net/http"
)



func main() {

    http.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
        fmt.Fprintf(w, "<html><head><link rel='stylesheet' type='text/css' href='style.css'><title>My Website</title><link rel='icon' href='static/image/favicon.png' type='image/x-icon'></head><body><h1>Welcome to my website!</h1></body></html>")
    })
    
    http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir("static"))))
    http.HandleFunc("/style.css", func(w http.ResponseWriter, r *http.Request) {
        css := `
            h1 { 
                color: wheat; text-align: center;
            }
            body { 
                background-repeat: no-repeat; background-attachment: fixed; background-size: cover; 
                background-position-y: center; background-image: url("/static/image/Background.jpg");
            }
        `
        w.Header().Set("Content-Type", "text/css")
        fmt.Fprint(w, css)
    })
    
    http.ListenAndServe(":80", nil)
}
