FROM golang:1.21.3-windowsservercore-1809 

# Add some labels
LABEL author='Matthieu'
LABEL description='This image will be used to display a webpage'



# Create a non-root User called "app"
#RUN groupadd -r app && useradd -r -g app app

# Copy of the app folder
COPY . /app

# This allows us to change the owner of files and connect as the user with non root rights that we will use for next commands
#RUN chown app:app -R /app && chown app:app -R /home
#USER app


WORKDIR /app

EXPOSE 80


CMD ["go", "run", "./website.go"]
